console.log("I'm Ready!");
//Names and Input
//1
var hacker1 = "Teresa";
//2
console.log("The driver's name is "+hacker1);
//3
var hacker2 = prompt("What is the navigator's name?");
//4
console.log("The navigator's name is "+hacker2);
//Conditionals
//5
if(hacker1.length>hacker2.length) {
  console.log("The Driver has the longest name, it has " + hacker1.length + "characters");
} else if (hacker1.length<hacker2.length) {
  console.log("Yo, navigator got the longest name, it has  " + hacker2.length + "characters");
} else {
  console.log("wow, you both got equally long names, " + hacker1.length + "characters!!");
}
//Loops
//6
for (var i=0;i<=hacker1.length-1; i++){
  console.log(hacker1[i].toUpperCase());
  console.log("");
}
//7
for (var j=hacker2.length-1; j>=0; j--){
  console.log(hacker2[j]);
  console.log("");
}
//8

//Bonus
//9 Palindrome=same word or string both ways

//10
var lorem="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam iaculis metus velit, vitae suscipit dolor dictum sed. Donec pulvinar faucibus auctor. Vestibulum in congue massa. In vitae massa justo. Cras fermentum ante nulla, ac mattis lectus lobortis at. Cras varius faucibus justo, id maximus nisl posuere sit amet. Phasellus eu tellus hendrerit mi porta finibus eu non tellus. Donec eu justo ut justo consectetur iaculis ac eget arcu./nIn suscipit urna quis neque tincidunt, vitae ornare mi suscipit. Vestibulum lobortis cursus sem consequat dictum. Sed commodo quis lacus nec faucibus. Curabitur tempus erat sit amet purus consequat, non facilisis neque aliquet. Cras gravida purus purus, a gravida enim commodo eu. Vestibulum dolor dui, pellentesque non gravida eget, volutpat non nunc. Vestibulum fermentum tortor at finibus sagittis. Suspendisse potenti. Aenean scelerisque, felis eu dapibus venenatis, tellus dolor ullamcorper libero, vel eleifend nisi enim nec dui. Nullam laoreet dignissim erat, et elementum leo. Quisque eu augue tortor. Mauris vitae iaculis ante, quis lacinia nunc./nDonec eu dui eu metus luctus finibus at nec dolor. Fusce massa sapien, tincidunt nec fringilla vel, fermentum ac mi. Nam mollis, nulla ac hendrerit porttitor, odio lacus convallis diam, vel iaculis leo turpis quis est. Nulla magna eros, ultrices non est sit amet, ultrices bibendum augue. Fusce sed arcu arcu. Aliquam cursus lobortis metus a blandit. Vivamus eget ante at lectus fermentum condimentum. Nulla ultrices tempor dignissim. Sed eget nisi in felis posuere suscipit. Suspendisse feugiat nibh eget ullamcorper pretium. Sed eget purus vel lorem aliquet vulputate."
//Count number of words in the string
function wordCount(str) { 
  return str.split(" ").length;
}
console.log(wordCount(lorem));
//Count number of times "et" appears
var count = 0;
var pos = lorem.indexOf("et");
while(pos > -1){
    ++count;
    pos = lorem.indexOf("et", ++pos);
}
console.log(count);  
